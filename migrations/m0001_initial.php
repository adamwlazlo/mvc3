<?php

class m0001_initial
{
    public function up()
    {
        echo 'Applying migration' . PHP_EOL;
        $db = \app\core\Application::$app->db;
        $SQL = "CREATE TABLE users (
                id INT AUTO_INCREMENT PRIMARY KEY,
                email VARCHAR(255) NOT NULL,
                name VARCHAR(255) NOT NULL,
                surname VARCHAR(255) NOT NULL,
                status TINYINT NOT NULL DEFAULT 0,
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
            ) ENGINE = INNODB;";
        $db->pdo->exec($SQL);
    }

    public function down()
    {
        echo 'Down migration' . PHP_EOL;
        $db = \app\core\Application::$app->db;
        $SQL = "DROP TABLE users;";
        $db->pdo->exec($SQL);
    }
}