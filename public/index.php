<?php

function pd($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}

function pde($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
    exit;
}

require_once dirname(__DIR__) . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

use app\core\Application;
use app\controllers\SitesController;
use app\controllers\AuthController;

$config = [
    'userClass' => \app\models\User::class,
    'db' => [
        'dsn' => $_ENV['DB_DSN'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASSWORD'],
    ]
];

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$app = new Application(dirname(__DIR__), $config);

$app->router->get('/', [SitesController::class, 'home']);
$app->router->get('/contact', [SitesController::class, 'contact']);
$app->router->post('/contact', [SitesController::class, 'contact']);
//{
//    return 'wysłane';
//});
//$app->router->post('/contact', [SitesController::class, 'handleContact']);

$app->router->get('/login', [AuthController::class, 'login']);
$app->router->post('/login', [AuthController::class, 'login']);
$app->router->get('/register', [AuthController::class, 'register']);
$app->router->post('/register', [AuthController::class, 'register']);
$app->router->get('/logout', [AuthController::class, 'logout']);
$app->router->get('/profile', [AuthController::class, 'profile']);

$app->run();