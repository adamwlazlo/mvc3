<?php


namespace app\core\exception;


use Throwable;

class NotFoundException extends \Exception
{
    protected $message = 'This site doesn\'t exist';
    protected $code = 404;
}