<?php

function pd($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}

function pde($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
    exit;
}

use app\core\Application;

require_once __DIR__ . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$config = [
    'db' => [
        'dsn' => $_ENV['DB_DSN'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASSWORD'],
    ]
];

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$app = new Application(__DIR__, $config);

$app->db->applyMigrations();